//
//  ViewController.swift
//  AlexGrinbergAssignment
//
//  Created by Alex Grinberg on 11/03/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD
import SVProgressHUD

class MainVCPageRepository: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    //Main Elements of the Main Page of the Repository App
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //Arrays for the data to be presented
    var repositoryArray = [[String:Any]]()
    var favoriteArray = [[String:Any]]()
    var pagecount = 1
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return repositoryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        <#code#>
    }
    
    func callAPI() {
        
        var currDate = Date()
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            currDate = NSCalendar.current.date(byAdding: .day, value: -1, to: Date())!
        case 1:
            currDate = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: Date())))!
        case 2:
            currDate = Calendar.current.date(from: Calendar.current.dateComponents([.year], from: Calendar.current.startOfDay(for: Date())))!
        default:
            break
        }
        
        let formater = DateFormatter()
        formater.dateFormat = "yyyy-MM-dd"
        let dateStr = formater.string(from: currDate)
        print(dateStr)
        
        loadingProgress(self.view, Show: true)
        let url = URL(string: "https://api.github.com/search/repositories?q=created%3A%3E\(dateStr)&sort=stars&order=desc&page=\(pagecount)")!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let firstData = data else { return }
            let jsonStr = String(data: firstData, encoding: .utf8)!
            guard let data = jsonStr.data(using: .utf8) else {
                return
            }
            do {
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                    return
                }
                print(json)
                let arr = (json["items"] as? [[String:Any]])!
                
                for obj in arr {
                    
                    self.repositoryArray.append(obj)
                }
                
                //                print(self.arrRepo)
                let when = DispatchTime.now()
                
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    self.loadingProgress(self.view, Show: false)
                    self.collectionView.reloadData()
                }
                
                
                //                var result = [[[String:Any]]]()
                //
                //                if let created = json["created"] as? [[String:Any]] {
                //
                //                    result.append(created)
                //
                //                }
                //                if let reshared = json["reshared"] as? [[String:Any]] {
                //
                //                    result.append(reshared)
                //
                //                }
                //
                //                print(result)
                //
                
            } catch {
                self.loadingProgress(self.view, Show: false)
                print(error.localizedDescription)
            }
        }
        
        task.resume()
    }
    
    func loadingProgress(_ view:UIView,Show:Bool){
        
        if Show {
            
            let hud = MBProgressHUD.showAdded(to: view, animated: true)
            hud.label.text = "Loading"
            
        }
        else{
            
            MBProgressHUD.hide(for: view, animated: true)
        }
    }
    

}

