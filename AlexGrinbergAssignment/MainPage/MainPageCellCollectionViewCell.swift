//
//  MainPageCellCollectionViewCell.swift
//  AlexGrinbergAssignment
//
//  Created by Alex Grinberg on 11/03/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class MainPageCellCollectionViewCell: UICollectionViewCell {
    
    //Main elements of the collection view cell
    @IBOutlet weak var sideImage: UIImageView!
    @IBOutlet var labels: [UILabel]!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var starButton: UIButton!
    
    
    
}
